build_industrial_expansion = {
	allowed = {
		always = yes
	}

	enable = {
		any_controlled_state = {
			free_building_slots = {
				building = industrial_complex
				size < 2
				include_locked = no
			}
		}
	}

	abort = {
		any_controlled_state = {
			free_building_slots = {
				building = industrial_complex
				size > 5
				include_locked = no
			}
		}
	}

	ai_strategy = {
		type = building_target
		id = industrial_expansion
		value = 150
	}
}